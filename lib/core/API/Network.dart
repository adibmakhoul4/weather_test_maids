import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:weather_test/core/API/Url.dart';
import 'package:weather_test/featuers/weather%20condition/data/ErrorModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/weatherDetailsModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/weatherModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/weatherResponseModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/windDetailsModel.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class Network {
  static var dio;
  static Response response;
  static Response response1;

  static initial() {
    BaseOptions options = new BaseOptions(headers: {
      "Accept-Language": 'en',
      'Authorization': null,
    });
    dio = Dio(options);
    dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
  }

  static Future<WeatherResponseModel> getWeatherCondition() async {
    WeatherResponseModel weatherResponseModel;
    List listweather = [];
    WeatherDetailsModel weatherDetailsModel;
    List weather = [];
    WindDetailsModel windDetailsModel;
    WeatherModel weatherModel;
    String date;
    String city;
    List<WeatherModel> weatherModels = [];
    try {
      response = await dio.get(Url.getDataUrl);

      city = response.data['city']['name'];
      listweather = response.data['list'];
      for (int i = 0; i < listweather.length; i = i + 1) {
        weather = listweather[i]['weather'];
        weatherDetailsModel = WeatherDetailsModel.fromJson(weather[0]);
        windDetailsModel = WindDetailsModel.fromJson(listweather[i]['wind']);
        weatherModel = WeatherModel.fromJson(listweather[i]['main']);
        date = listweather[i]['dt_txt'];
        weatherModel.weatherDetailsModel = weatherDetailsModel;
        weatherModel.windDetailsModel = windDetailsModel;
        weatherModel.date = DateTime.parse(date);
        weatherModels.add(weatherModel);
      }
      weatherResponseModel = WeatherResponseModel.fromJson(
          response?.data ?? WeatherResponseModel(isOk: false));
      weatherResponseModel.isOk = true;
      weatherResponseModel.weatherModels = weatherModels;
      weatherResponseModel.city = city;
    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout) {
        weatherResponseModel = WeatherResponseModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Connect Timeout"));
      } else if (error.type == DioErrorType.sendTimeout) {
        weatherResponseModel = WeatherResponseModel(
            isOk: false, error: ErrorModel(code: 120, message: "Send Timeout"));
      } else if (error.type == DioErrorType.other) {
        weatherResponseModel = WeatherResponseModel(
            isOk: false, error: ErrorModel(code: 120, message: "Server error"));
      } else if (error.type == DioErrorType.cancel) {
        weatherResponseModel = WeatherResponseModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Connect cance"));
      } else if (error.type == DioErrorType.receiveTimeout) {
        weatherResponseModel = WeatherResponseModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Receive Timeout"));
      } else {
        weatherResponseModel = WeatherResponseModel.fromJson(
            error.response?.data ?? WeatherResponseModel(isOk: false));
      }
    } catch (e) {
      weatherResponseModel = WeatherResponseModel(
          error: ErrorModel(message: e.toString()), isOk: false);
    }
    return weatherResponseModel;
  }

  static Future<WeatherResponseModel> getWeatherConditionDetails(
      DateTime dateTime) async {
    WeatherResponseModel weatherResponseModel;
    List listweather = [];
    WeatherDetailsModel weatherDetailsModel;
    List weather = [];
    WindDetailsModel windDetailsModel;
    WeatherModel weatherModel;
    String dates;
    DateTime dateTimeOfObject;
    var totalHumidity = 0;
    var totalTemp = 0.0;
    var totalWind = 0.0;
    String city;
    List<WeatherModel> weatherModels = [];
    try {
      response = await dio.get(Url.getDataUrl);
      city = response.data['city']['name'];
      listweather = response.data['list'];
      for (int i = 0; i < listweather.length; i = i + 1) {
        weather = listweather[i]['weather'];
        weatherDetailsModel = WeatherDetailsModel.fromJson(weather[0]);
        windDetailsModel = WindDetailsModel.fromJson(listweather[i]['wind']);
        weatherModel = WeatherModel.fromJson(listweather[i]['main']);
        dates = listweather[i]['dt_txt'];
        dateTimeOfObject = DateTime.parse(dates);
        DateTime datse = new DateTime(dateTimeOfObject.year,
            dateTimeOfObject.month, dateTimeOfObject.day);
        DateTime datse1 =
            new DateTime(dateTime.year, dateTime.month, dateTime.day);
        weatherModel.windDetailsModel = windDetailsModel;
        weatherModel.weatherDetailsModel = weatherDetailsModel;
        if (datse == datse1) {
          weatherModel.date = dateTimeOfObject;
          weatherModels.add(weatherModel);
        }
      }
      for (int m = 0; m < weatherModels.length; m++) {
        totalHumidity = totalHumidity + weatherModels[m].humidity;
        totalTemp = totalTemp + weatherModels[m].temp;
        totalWind = totalWind + weatherModels[m].windDetailsModel.speed;
      }
      weatherModel.windDetailsModel = WindDetailsModel(speed: totalWind);
      weatherResponseModel = WeatherResponseModel.fromJson(
          response?.data ?? WeatherResponseModel(isOk: false));
      weatherResponseModel.isOk = true;
      weatherResponseModel.weatherModels = weatherModels;
      weatherResponseModel.humity = totalHumidity / weatherModels.length;
      weatherResponseModel.wind = totalWind / weatherModels.length;
      weatherResponseModel.temp = totalTemp / weatherModels.length;
      weatherResponseModel.city = city;
      weatherResponseModel.weather = weatherModels[0].weatherDetailsModel.main;
    } on DioError catch (error) {
      if (error.type == DioErrorType.connectTimeout) {
        weatherResponseModel = WeatherResponseModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Connect Timeout"));
      } else if (error.type == DioErrorType.sendTimeout) {
        weatherResponseModel = WeatherResponseModel(
            isOk: false, error: ErrorModel(code: 120, message: "Send Timeout"));
      } else if (error.type == DioErrorType.other) {
        weatherResponseModel = WeatherResponseModel(
            isOk: false, error: ErrorModel(code: 120, message: "Server error"));
      } else if (error.type == DioErrorType.cancel) {
        weatherResponseModel = WeatherResponseModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Connect cance"));
      } else if (error.type == DioErrorType.receiveTimeout) {
        weatherResponseModel = WeatherResponseModel(
            isOk: false,
            error: ErrorModel(code: 120, message: "Receive Timeout"));
      } else {
        weatherResponseModel = WeatherResponseModel.fromJson(
            error.response?.data ?? WeatherResponseModel(isOk: false));
      }
    } catch (e) {
      weatherResponseModel = WeatherResponseModel(
          error: ErrorModel(message: e.toString()), isOk: false);
    }
    return weatherResponseModel;
  }
}
