import 'dart:convert';
import 'dart:ffi';

import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  SharedPref._();
  SharedPreferences _preferences;
  static final SharedPref pref = SharedPref._();

  Future<SharedPreferences> get _getSharedPref async {
    if (_preferences != null)
      return _preferences;
    else {
      _preferences = await SharedPreferences.getInstance();
      return _preferences;
    }
  }

  saveToday(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  readToday(String key) async {
    final prefs = await SharedPreferences.getInstance();
    var response = prefs.getString(key);
    if (response != null) {
      return json.decode(prefs.getString(key));
    } else {
      return null;
    }
  }

  saveTommorow(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  readTommorow(String key) async {
    final prefs = await SharedPreferences.getInstance();
    var response = prefs.getString(key);
    if (response != null) {
      return json.decode(prefs.getString(key));
    } else {
      return null;
    }
  }

  saveAfter(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  readAfter(String key) async {
    final prefs = await SharedPreferences.getInstance();
    var response = prefs.getString(key);
    if (response != null) {
      return json.decode(prefs.getString(key));
    } else {
      return null;
    }
  }
}
