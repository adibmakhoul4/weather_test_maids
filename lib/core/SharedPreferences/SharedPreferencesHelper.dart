import 'dart:convert';

import 'package:weather_test/core/constants/app-string.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'SharedPreferencesProvider.dart';

class AppSharedPreferences {
  static bool initialized;
  static SharedPreferencesProvider _pref;
  static init() async {
    _pref = await SharedPreferencesProvider.getInstance();
  }

  //Lang
  static String get lang => _pref.read(AppStrings.LANG);
  static set lang(String lang) => _pref.save(AppStrings.LANG, lang);

  //first launch
  static bool get getFirstLunch {
    if (!_pref.contains(AppStrings.FIRSTLAUNCH)) {
      saveFirstLunch(true);
      return true;
    } else {
      return false;
    }
  }

  static saveFirstLunch(bool value) =>
      _pref.save(AppStrings.FIRSTLAUNCH, value);

  // save today
  static String get getToday => _pref.read(AppStrings.TODAY);
  saveToday(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  readToday(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(key));
  }

  // save TOMMOROW
  static String get getTommorow => _pref.read(AppStrings.TOMMOROW);
  static saveTommorow(String tommorow) =>
      _pref.save(AppStrings.TOMMOROW, tommorow);
  // save today
  static String get getAfter => _pref.read(AppStrings.AFTER);
  static saveAfter(String after) => _pref.save(AppStrings.AFTER, after);
}
