class AppStrings {
  AppStrings._();
  static const LANG = "LANG";
  static const OTP = "OTP";
  static String TODAY = "TODAY";
  static String TOMMOROW  = "TOMMOROW";
  static String AFTER = "AFTER";
  static const FIRSTLAUNCH = "FIRSTLAUNCH";
}
