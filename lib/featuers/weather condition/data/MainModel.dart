class MainModel {
  var temp;
  var humidity;
  MainModel({this.temp, this.humidity});

  MainModel.fromJson(Map<String, dynamic> json) {
    temp = json['temp'];
    humidity = json['humidity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['temp'] = this.temp;
    data['humidity'] = this.humidity;
    return data;
  }
}
