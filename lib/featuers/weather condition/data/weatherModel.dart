import 'package:weather_test/featuers/weather%20condition/data/weatherDetailsModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/windDetailsModel.dart';

class WeatherModel {
  var temp;
  var humidity;
  var wind;

  DateTime date;
  WindDetailsModel windDetailsModel;
  WeatherDetailsModel weatherDetailsModel;
  WeatherModel({this.temp, this.humidity, this.date});

  WeatherModel.fromJson(Map<String, dynamic> json) {
    temp = json['temp'];
    humidity = json['humidity'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['temp'] = this.temp;
    data['humidity'] = this.humidity;
    data['date'] = this.date;

    return data;
  }
}
