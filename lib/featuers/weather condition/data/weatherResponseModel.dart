import 'package:weather_test/featuers/weather%20condition/data/ErrorModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/weatherDetailsModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/weatherModel.dart';

class WeatherResponseModel {
  bool isOk;
  ErrorModel error;
  var humity;
  double temp;
  double wind;
  String weather;
  var city;
  List<WeatherModel> weatherModels;

  WeatherResponseModel({
    this.isOk,
    this.error,
    this.weatherModels,
  });

  WeatherResponseModel.fromJson(Map<String, dynamic> json) {
    isOk = json['isOk'];
    error =
        json['error'] != null ? new ErrorModel.fromJson(json['error']) : null;
    temp = json['temp'];
    humity = json['humity'];
    wind = json['wind'];
    weather = json['weather'];
    city = json['city'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isOk'] = this.isOk;
    if (this.error != null) {
      data['error'] = this.error.toJson();
    }
    data['temp'] = this.temp;
    data['humity'] = this.humity;
    data['wind'] = this.wind;
    data['weather'] = this.weather;
    data['city'] = this.city;

    return data;
  }
}
