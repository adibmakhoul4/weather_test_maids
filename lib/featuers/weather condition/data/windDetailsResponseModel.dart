import 'package:weather_test/featuers/weather%20condition/data/ErrorModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/weatherDetailsModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/windDetailsModel.dart';

class WindDetailsResponseModel {
  bool isOk;
  ErrorModel error;
  WindDetailsModel windDetailsModel;

  WindDetailsResponseModel({
    this.isOk,
    this.error,
    this.windDetailsModel,
  });

  WindDetailsResponseModel.fromJson(Map<String, dynamic> json) {
    isOk = json['isOk'];
    error =
        json['error'] != null ? new ErrorModel.fromJson(json['error']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['isOk'] = this.isOk;
    if (this.error != null) {
      data['error'] = this.error.toJson();
    }
    return data;
  }
}
