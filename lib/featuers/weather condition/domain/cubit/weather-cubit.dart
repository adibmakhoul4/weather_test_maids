library my_lib;

import 'dart:convert';

import 'package:weather_test/core/API/Network.dart';
import 'package:weather_test/core/SharedPreferences/SharedPref.dart';
import 'package:weather_test/featuers/weather%20condition/data/ErrorModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/weatherDetailsResponseModel.dart';
import 'package:weather_test/featuers/weather%20condition/data/weatherResponseModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'weather-state.dart';

class WeatherConditionCubit extends Cubit<WeatherConditionCubitState> {
  WeatherConditionCubit() : super(UserMangmentInitial());

  void getWeatherCondition({BuildContext context}) async {
    WeatherResponseModel weatherResponseModel;
    emit(GetWeatherLoading());
    try {
      weatherResponseModel = await Network.getWeatherCondition();
      if (weatherResponseModel.isOk ?? false) {
        Network.initial();
        emit(GetWeatherSucess(
            weatherDetailsResponseModel: weatherResponseModel));
      } else {
        emit(GetWeatherFailur(
            errorModel:
                weatherResponseModel.error ?? ErrorModel(message: 'Error')));
      }
    } catch (e) {
      print(e.toString());
      emit(GetWeatherFailur(errorModel: ErrorModel(message: e.toString())));
    }
  }

  void getWeatherConditionDetails(DateTime dateTime) async {
    WeatherResponseModel weatherResponseModel;
    emit(GetWeatherLoading());
    try {
      weatherResponseModel = await Network.getWeatherConditionDetails(dateTime);
      if (weatherResponseModel.isOk ?? false) {
        Network.initial();
        setLocalData(dateTime, weatherResponseModel);
        emit(GetWeatherSucess(
            weatherDetailsResponseModel: weatherResponseModel));
      } else {
        emit(GetWeatherFailur(
            errorModel:
                weatherResponseModel.error ?? ErrorModel(message: 'Error')));
      }
    } catch (e) {
      print(e.toString());
      emit(GetWeatherFailur(errorModel: ErrorModel(message: e.toString())));
    }
  }

  void getWeatherlocal(DateTime dateTime) async {
    WeatherResponseModel weatherResponseModel;
    emit(GetWeatherLoading());
    try {
      weatherResponseModel = await getLocalData(dateTime);
      if (weatherResponseModel.isOk ?? false) {
        emit(GetWeatherLocalSucess(
            weatherDetailsResponseModel: weatherResponseModel));
      } else {
        emit(GetWeatherLocalFailur(
            errorModel:
                weatherResponseModel.error ?? ErrorModel(message: 'Error')));
      }
    } catch (e) {
      print(e.toString());
      emit(
          GetWeatherLocalFailur(errorModel: ErrorModel(message: e.toString())));
    }
  }

  Future<WeatherResponseModel> getLocalData(DateTime dateTime) async {
    WeatherResponseModel weatherResponseModel;
    DateTime now = new DateTime.now();
    DateTime datenow = new DateTime(now.year, now.month, now.day);

    DateTime tomorrow = DateTime.now().add(Duration(days: 1));
    DateTime datetomorrow =
        new DateTime(tomorrow.year, tomorrow.month, tomorrow.day);

    DateTime after = DateTime.now().add(Duration(days: 2));
    DateTime dateafter = new DateTime(after.year, after.month, after.day);
    if (dateTime == datenow) {
      weatherResponseModel = WeatherResponseModel.fromJson(
          await SharedPref.pref.readToday("today"));
    } else if (dateTime == datetomorrow) {
      weatherResponseModel = WeatherResponseModel.fromJson(
          await SharedPref.pref.readTommorow("tomorrow"));
    } else if (dateTime == dateafter) {
      weatherResponseModel = WeatherResponseModel.fromJson(
          await SharedPref.pref.readAfter("after"));
    }
    return weatherResponseModel;
  }

  setLocalData(
      DateTime dateTime, WeatherResponseModel weatherResponseModel) async {
    DateTime now = new DateTime.now();
    DateTime datenow = new DateTime(now.year, now.month, now.day);

    DateTime tomorrow = DateTime.now().add(Duration(days: 1));
    DateTime datetomorrow =
        new DateTime(tomorrow.year, tomorrow.month, tomorrow.day);

    DateTime after = DateTime.now().add(Duration(days: 2));
    DateTime dateafter = new DateTime(after.year, after.month, after.day);
    if (dateTime == datenow) {
      SharedPref.pref.saveToday("today", weatherResponseModel);
    } else if (dateTime == datetomorrow) {
      SharedPref.pref.saveTommorow("tomorrow", weatherResponseModel);
    } else if (dateTime == dateafter) {
      SharedPref.pref.saveAfter("after", weatherResponseModel);
    }
  }
}
