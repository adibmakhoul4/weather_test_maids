part of my_lib;

@immutable
abstract class WeatherConditionCubitState {}

class UserMangmentInitial extends WeatherConditionCubitState {}

class GetWeatherLoading extends WeatherConditionCubitState {}

class GetWeatherSucess extends WeatherConditionCubitState {
  final WeatherResponseModel weatherDetailsResponseModel;
  GetWeatherSucess({this.weatherDetailsResponseModel});
}

class GetWeatherFailur extends WeatherConditionCubitState {
  final ErrorModel errorModel;
  GetWeatherFailur({this.errorModel});
}

class GetWeatherLocalSucess extends WeatherConditionCubitState {
  final WeatherResponseModel weatherDetailsResponseModel;
  GetWeatherLocalSucess({this.weatherDetailsResponseModel});
}

class GetWeatherLocalFailur extends WeatherConditionCubitState {
  final ErrorModel errorModel;
  GetWeatherLocalFailur({this.errorModel});
}
