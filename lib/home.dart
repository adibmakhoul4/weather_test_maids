import 'dart:ui';

import 'package:intl/intl.dart';
import 'package:weather_test/Animation/FadeAnimation.dart';
import 'package:weather_test/weather-details.dart';
import 'package:weather_test/featuers/weather%20condition/data/weatherModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/API/Network.dart';
import 'featuers/weather condition/domain/cubit/weather-cubit.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  WeatherConditionCubit weatherConditionCubit = new WeatherConditionCubit();

  @override
  initState() {
    super.initState();
    weatherConditionCubit.getWeatherCondition();
  }

  @override
  Widget build(BuildContext context) {
    DateTime now = new DateTime.now();
    DateTime datenow = new DateTime(now.year, now.month, now.day);

    DateTime tomorrow = DateTime.now().add(Duration(days: 1));
    DateTime datetomorrow =
        new DateTime(tomorrow.year, tomorrow.month, tomorrow.day);

    DateTime after = DateTime.now().add(Duration(days: 2));
    DateTime dateafter = new DateTime(after.year, after.month, after.day);

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: null,
        centerTitle: true,
        title: Text(
          "Weather",
          style: TextStyle(
              fontFamily: 'Poppins', fontSize: 25.0, color: Colors.black),
        ),
        brightness: Brightness.light,
      ),
      body: BlocProvider(
          create: (context) => weatherConditionCubit,
          child:
              BlocConsumer<WeatherConditionCubit, WeatherConditionCubitState>(
                  listener: (context, state) {
            if (state is GetWeatherLoading) {
              return Center(
                child: CircularProgressIndicator(
                  color: Colors.orange,
                ),
              );
            }
            if (state is GetWeatherFailur) {
              return Scaffold.of(context).showSnackBar(SnackBar(
                content: Text(
                  "Something went wrong!",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
                duration: Duration(seconds: 2),
                backgroundColor: Colors.red,
              ));
            }
          }, builder: (context, state) {
            if (state is GetWeatherSucess) {
              return SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    children: <Widget>[
                      FadeAnimation(
                          1.5,
                          makeItem(
                              image: 'assets/images/Thunderstorm.gif',
                              date: datenow,
                              tag: 'red',
                              context: context)),
                      FadeAnimation(
                          1.6,
                          makeItem(
                              image: 'assets/images/cloudy.gif',
                              date: datetomorrow,
                              tag: 'blue',
                              context: context)),
                      FadeAnimation(
                          1.7,
                          makeItem(
                              image: 'assets/images/rainy.gif',
                              date: dateafter,
                              tag: 'white',
                              context: context)),
                    ],
                  ),
                ),
              );
            }
            if (state is GetWeatherLoading) {
              return Center(
                child: CircularProgressIndicator(
                  color: Colors.orange,
                ),
              );
            }
            return Container();
          })),
    );
  }

  Widget makeItem({ image, tag, date, context}) {
    return Hero(
      tag: tag,
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Weather(
                        image: image,
                        date: date,
                        tag:tag
                      )));
        },
        child: Container(
          height: 140,
          width: double.infinity,
          padding: EdgeInsets.all(20),
          margin: EdgeInsets.only(bottom: 20),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              image:
                  DecorationImage(image: AssetImage(image), fit: BoxFit.cover),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey[400],
                    blurRadius: 10,
                    offset: Offset(0, 10))
              ]),
          child: ClipRRect(
            // make sure we apply clip it properly
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
              child: Container(
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              FadeAnimation(
                                  1,
                                  Text(
                                    DateFormat('EEEE').format(date),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold),
                                  )),
                              SizedBox(
                                height: 10,
                              ),
                              FadeAnimation(
                                  1.1,
                                  Text(
                                    date.toString().substring(0, 10),
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 18),
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // FadeAnimation(1.2, Text(object.temp.toString(), style: TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold),)),
                        // FadeAnimation(1.2, Text(object.wind.toString(), style: TextStyle(color: Colors.white, fontSize: 22, fontWeight: FontWeight.bold),)),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
