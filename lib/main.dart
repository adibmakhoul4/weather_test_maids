import 'package:weather_test/splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'core/API/Network.dart';
import 'core/SharedPreferences/SharedPreferencesHelper.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppSharedPreferences.init();

  Network.initial();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Weather',
      theme: ThemeData(fontFamily: 'Poppins'),
      darkTheme: ThemeData(
          brightness: Brightness.dark, primarySwatch: Colors.deepOrange),
      home: SplashScreen(),
    );
  }
}
