import 'package:weather_test/Animation/FadeAnimation.dart';
import 'package:weather_test/core/SharedPreferences/SharedPref.dart';
import 'package:weather_test/core/SharedPreferences/SharedPreferencesHelper.dart';
import 'package:weather_test/featuers/weather%20condition/data/weatherModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'featuers/weather condition/data/weatherResponseModel.dart';
import 'featuers/weather condition/domain/cubit/weather-cubit.dart';

class Weather extends StatefulWidget {
  final String image;
  final DateTime date;
  final String tag;
  const Weather({Key key, this.image, this.date,this.tag})
      : super(key: key);

  @override
  _WeatherState createState() => _WeatherState();
}

class _WeatherState extends State<Weather> {
  WeatherConditionCubit weatherConditionCubit = new WeatherConditionCubit();
  SharedPref sharedPref;
  var dataToday;
  var dataTommmorow;
  var dataAfter;
  String imageBackground;
  WeatherModel weatherModelnew;
  WeatherResponseModel weatherResponseModel;
    DateTime now = new DateTime.now();
DateTime tomorrow = DateTime.now().add(Duration(days:1));
DateTime after = DateTime.now().add(Duration(days:2));

checkDataSource() async {
   dataToday =await SharedPref.pref.readToday("today");
   dataTommmorow =await SharedPref.pref.readTommorow("tomorrow");
   dataAfter =await SharedPref.pref.readAfter("after");
   if(widget.date == DateTime(now.year, now.month, now.day)){
 if(dataToday!=null){
      weatherConditionCubit.getWeatherlocal(widget.date);
    }else {
weatherConditionCubit.getWeatherConditionDetails(widget.date);
    }
   }else if(widget.date == DateTime(tomorrow.year, tomorrow.month, tomorrow.day)){
 if(dataTommmorow!=null){
      weatherConditionCubit.getWeatherlocal(widget.date);
    }else {
weatherConditionCubit.getWeatherConditionDetails(widget.date);
    }
   }else if(widget.date == DateTime(after.year, after.month, after.day)){
 if(dataAfter!=null){
      weatherConditionCubit.getWeatherlocal(widget.date);
    }else {
weatherConditionCubit.getWeatherConditionDetails(widget.date);
    }
   }
}
  @override
  initState() {
    super.initState();
checkDataSource();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocProvider(
            create: (context) => weatherConditionCubit,
            child:
                BlocConsumer<WeatherConditionCubit, WeatherConditionCubitState>(
                    listener: (context, state) {
              if (state is GetWeatherLoading) {
                return Center(
                  child: CircularProgressIndicator(
                    color: Colors.orange,
                  ),
                );
              }
                 if (state is GetWeatherFailur) {
                 return  Scaffold.of(context).showSnackBar(
     SnackBar(content: Text("Something went wrong!",
     textAlign: TextAlign.center, style: TextStyle(fontSize: 16.0, fontWeight: 
     FontWeight.bold),), duration: Duration(seconds: 2), backgroundColor: Colors.red,)
);
                  }

            }, builder: (context, state) {
              if (state is GetWeatherSucess) {
                  return SingleChildScrollView(
                    child: Hero(
                  tag: widget.tag,
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(setWeatherImg(state.weatherDetailsResponseModel.weather)), fit: BoxFit.cover),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[400],
                              blurRadius: 10,
                              offset: Offset(0, 10))
                        ]),
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 50),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                       
                            ],
                          ),
                        ),
                        Positioned(
                          top: 80,
                          left:10,
                          width: MediaQuery.of(context).size.width,
                          child: FadeAnimation(
                              1.3,
                              Text(
                                state.weatherDetailsResponseModel
                                    .weather,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 40,
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          width: MediaQuery.of(context).size.width,
                          height: 500,
                          child: FadeAnimation(
                              1,
                              Container(
                                padding: EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.bottomRight,
                                        colors: [
                                      Colors.black.withOpacity(.9),
                                      Colors.black.withOpacity(.0),
                                    ])),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    FadeAnimation(
                                        1.3,
                                        Text(
                                          state.weatherDetailsResponseModel.city,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 40,
                                              fontWeight: FontWeight.bold),
                                        )),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Container(
                                      height: 100,
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children: <Widget>[
                                          FadeAnimation(
                                                    1,
                                                    Text(
                                                      state
                                                          .weatherDetailsResponseModel
                                                          .temp
                                                          .toInt()
                                                          .toString()+'°',
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 78,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )),
                                             
                                         
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Container(
                                              height: 300,
                                              width: 150,
                                              child: Card(
                                                color: Colors.white,
                                                child: FadeAnimation(
                                                  1,
                                                  Center(
                                                    child: new Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: <Widget>[
                                                        new Text('Wind'),
                                                        new Container(
                                                          padding:
                                                              new EdgeInsets
                                                                      .only(
                                                                  top: 16.0),
                                                          child: new Row(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              new Text(
                                                                state
                                                                    .weatherDetailsResponseModel
                                                                    .wind
                                                                    .toInt()
                                                                    .toString()+'km/h',
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                        30.0,
                                                                    fontWeight: FontWeight.bold),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              )),
                                          Container(
                                              height: 300,
                                              width: 150,
                                              child: Card(
                                                color: Colors.white,
                                                child: FadeAnimation(
                                                  1,
                                                  Center(
                                                    child: new Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: <Widget>[
                                                        new Text('Humidity'),
                                                        new Container(
                                                          padding:
                                                              new EdgeInsets
                                                                      .only(
                                                                  top: 16.0),
                                                          child: new Row(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              new Text(
                                                                state
                                                                    .weatherDetailsResponseModel
                                                                    .humity
                                                                     .toInt()
                                                                    .toString()+'%',
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                        30.0,
                                                                    fontWeight: FontWeight.bold),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ));
               }
              if (state is GetWeatherLoading) {
                return Center(
                  child: CircularProgressIndicator(
                    color: Colors.orange,
                  ),
                );
              }
                 if (state is GetWeatherLocalSucess) {
                return SingleChildScrollView(
                    child: Hero(
                  tag: widget.tag,
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(setWeatherImg(state.weatherDetailsResponseModel.weather)), fit: BoxFit.cover),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[400],
                              blurRadius: 10,
                              offset: Offset(0, 10))
                        ]),
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 50),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                       
                            ],
                          ),
                        ),
                        Positioned(
                          top: 80,
                          left:10,
                          width: MediaQuery.of(context).size.width,
                          child: FadeAnimation(
                              1.3,
                              Text(
                                state.weatherDetailsResponseModel
                                    .weather,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 50,
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                        Positioned(
                          bottom: 0,
                          left: 0,
                          width: MediaQuery.of(context).size.width,
                          height: 500,
                          child: FadeAnimation(
                              1,
                              Container(
                                padding: EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.bottomRight,
                                        colors: [
                                      Colors.black.withOpacity(.9),
                                      Colors.black.withOpacity(.0),
                                    ])),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    FadeAnimation(
                                        1.3,
                                        Text(
                                          state.weatherDetailsResponseModel.city,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 40,
                                              fontWeight: FontWeight.bold),
                                        )),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Container(
                                      height: 100,
                                      child: ListView(
                                        scrollDirection: Axis.horizontal,
                                        children: <Widget>[
                                          FadeAnimation(
                                                    1,
                                                    Text(
                                                      state
                                                          .weatherDetailsResponseModel
                                                          .temp
                                                          .toInt()
                                                          .toString()+'°',
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 78,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    )),
                                             
                                        
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Container(
                                              height: 300,
                                              width: 150,
                                              child: Card(
                                                color: Colors.white,
                                                child: FadeAnimation(
                                                  1,
                                                  Center(
                                                    child: new Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: <Widget>[
                                                        new Text('Wind'),
                                                        new Container(
                                                          padding:
                                                              new EdgeInsets
                                                                      .only(
                                                                  top: 16.0),
                                                          child: new Row(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              new Text(
                                                                state
                                                                    .weatherDetailsResponseModel
                                                                    .wind
                                                                    .toInt()
                                                                    .toString()+'km/h',
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                        30.0,
                                                                    fontWeight:FontWeight.bold
                                                              ))
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              )),
                                          Container(
                                              height: 300,
                                              width: 150,
                                              child: Card(
                                                color: Colors.white,
                                                child: FadeAnimation(
                                                  1,
                                                  Center(
                                                    child: new Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: <Widget>[
                                                        new Text('Humidity'),
                                                        new Container(
                                                          padding:
                                                              new EdgeInsets
                                                                      .only(
                                                                  top: 16.0),
                                                          child: new Row(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              new Text(
                                                                state
                                                                    .weatherDetailsResponseModel
                                                                    .humity
                                                                     .toInt()
                                                                    .toString(),
                                                                style: new TextStyle(
                                                                    fontSize:
                                                                        30.0,
                                                                    fontWeight:FontWeight.bold
                                                              )
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ))
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                ));
              }
            
              return Container();
            })));
  }
  setWeatherImg(String weather) {

  switch (weather) {
    case 'Clouds':
      // do something
             imageBackground ='assets/images/cloudy.gif';

      break;
      case 'Clear':
      // do something
             imageBackground ='assets/images/sunny.gif';

      break;
            case 'Thunderstorm':
      // do something
             imageBackground ='assets/images/Thunderstorm.gif';

      break;
            case 'Snow':
      // do something
             imageBackground ='assets/images/snow.gif';

      break;
            case 'Drizzle':
      // do something
             imageBackground ='assets/images/drizzle.gif';

      break;
            case 'Atmosphere':
      // do something
             imageBackground ='assets/images/atmosphere.gif';

      break;
               case 'Rainy':
      // do something
             imageBackground ='assets/images/rainy.gif';

      break;
      // do something else
      default:
       imageBackground ='assets/images/atmosphere.jpg';

      break;
  }
  return imageBackground;
}
}
